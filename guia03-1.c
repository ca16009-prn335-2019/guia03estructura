#include <stdio.h>

void main()
{
    int opcion;
    int a, b, r;
    int *p1;
    int *p2;
    int *p3;

    do
    {
        printf( "\n¿Qué desea realizar?" );
        printf( "\n\n   1. Sumar dos numeros.");
        printf( "\n   2. Restar dos numeros." );
        printf( "\n   3. Ver direcciones en memoria de las variables." );
        printf( "\n   4. Salir.\n" );

        
        do
        {
            scanf( "%d", &opcion);

        } while ( opcion < 1 || opcion > 4 );
        

        switch ( opcion )
        {
                     
            case 1: printf( "\n   Ingrese el primer numero: " );
                      scanf( "%d", &a);
                      p1=&a;
                      printf( "\n   Ingrese el segundo numero: " );
                      scanf( "%d", &b);
                      p2=&b;
                      r=a+b;
                      p3=&r;
                      printf( "\n %d + %d = %d\n",*p1,*p2, *p1+*p2);
                      printf( "\n Resultado de la suma: %d\n", r );
                      
                      break;

                      
            case 2: printf( "\n   Ingrese el primer numero: " );
                      p1=&a;
                      scanf( "%d", &a);
                      p2=&b;
                      printf( "\n   Ingrese el segundo numero: " );
                      scanf( "%d", &b);
                      r=a-b;
                      p3=&r;
                      printf( "\n %d - %d = %d\n",*p1,*p2, *p1-*p2);
                      printf( "\n Resultado de la resta: %d\n", r );
                      break;
                      
                      
			case 3:  p1=&a;
                      p2=&b;
                      p3=&r;
                      printf("\n La direccion de n1 es %p\n La direccion de n2 es %p\n La direccion de r es %p\n\n", &p1, &p2, &p3);       
					  break;
			
			case 4: 
					printf("Adiós!\n");

        }
    } while ( opcion != 4 );
}
